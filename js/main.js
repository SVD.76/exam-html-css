'use strict';

document.addEventListener('DOMContentLoaded', function() {
    
    let burgerMenuElt = document.getElementById('burger-time');
    let menuListElt = document.getElementById('menu-list');
    
    burgerMenuElt.addEventListener('click', function() {
        
        menuListElt.classList.toggle('over-all');
        
    });
    
});